# VirusTracker
  
<a name='目录'></a>
  
## 目录
- [VirusTracker](#virustracker)
  * [目录](#目录)
  * [介绍](#介绍)
  * [软件架构](#软件架构)
  * [项目基于](#项目基于)
  * [安装依赖运行](#安装依赖运行)
  * [设置说明](#设置说明)
  * [开发说明](#开发说明)
    + [app.py 函数说明](#app.py函数说明)
      - [基础函数](#基础函数)
        * [需要手动调用的函数](#需要手动调用的函数)
          + [getHistory()](#getHistory)
          + [fetchOverallData()](#fetchOverallData)
          + [fetchAreaData()](#fetchAreaData)
        * [无需手动调用的函数](#无需手动调用的函数)
          + [getFileMD5(filename)](#getFileMD5)
          + [timeStamp(filename)](#timeStamp)
          + [getOverallData(data_parameter, region='domestic')](#getOverallData)
          + [historyReader(parameter)](#historyReader)
          + [getAreaData(parameter='confirmedCount')](#getAreaData)
      - [绘图函数](#绘图函数)
        * [createGeneralCharts()](#createGeneralCharts)
        * [createIncreaseCharts()](#createIncreaseCharts)
        * [createDatazone()](#createDatazone)
        * [createProvincialBar()](#createProvincialBar)
        * [createMap()](#createMap)
      - [功能函数](#功能函数)
    + [参数说明](#参数说明)
      - [fileChange](#fileChange)
      - [historyUpdated](#historyUpdated)
  * [参与贡献](#参与贡献)
  
  
<a name='介绍'></a>
  
## 介绍
基于Python的易用的新冠肺炎疫情追踪平台  
特点：按需更新 自动推送  
演示：[https://virustracker.guoch.xyz](https://virustracker.guoch.xyz "https://virustracker.guoch.xyz")  
在设置好Crontab后能定期自动抓取数据，更新网页，并保留所有数据点的可视化图表。  
许可证：CC-BY-SA 4.0 详见`LICENSE`文件  
注意：由于丁香园部分数据点会返回肉眼可见的噪声数据，部分图表可能因此而出现差错。您若发现可以提交issue给我们，并在[DXY-COVID-19-Crawler](https://github.com/BlankerL/DXY-COVID-19-Crawler "DXY-COVID-19-Crawler")中提交issue帮助更多的人。

<a name='软件架构'></a>
  
## 软件架构
软件架构说明  
本程序仅负责数据处理，绘制图表，网页由`WordPress`实现。
1. **拉取数据：**  
    由`requests`拉取
    [DXY-COVID-19-Crawler](https://github.com/BlankerL/DXY-COVID-19-Crawler "DXY-COVID-19-Crawler")
    提供的Api拉取2组数据：`Overall`及`Area`。
    同时拉取[DXY-COVID-19-Data](https://github.com/BlankerL/DXY-COVID-19-Data "DXY-COVID-19-Data")
    中以CSV格式存储的疫情数据序列。
2. **检测是否更新：**  
    - `Overall`及`Area`两组数据的特征值存储在`fileList.txt`中。  
        - `Overall`的特征值是返回数据中的更新时间戳
    （使用`getOverallData('updateTime', 'general')`取得）
        - `Area`数据的特征值为数据的MD5校验值  
        
        通过比较最新数据的特征值与文件中存储的特征值，得知是否更新。
    - 时间序列更新判断由`historyUpdated()`负责，具体原理请查看 [historyUpdated](#historyUpdated) 部分
    - 任何更新都会使参数`fileChange`变为`True`触发后续更新  
3. **渲染图表：**  
    请查看 [绘图函数](#绘图函数) 章节。
    除函数中所提到的位置外，所有图表均会被保存到`history`文件夹下的以时间戳为名的文件夹中。
4. **更新网页：**  
    调用调用python-wordpress-xmlrpc更新历史数据。最新数据页面中嵌入的图表始终为最新，无需更新网页。

<a name='项目基于'></a>
  
## 项目基于
- python3
- requests
- pandas
- pyecharts
- bleach
- python-wordpress-xmlrpc
- [DXY-COVID-19-Crawler](https://github.com/BlankerL/DXY-COVID-19-Crawler "DXY-COVID-19-Crawler")
- [WordPress](https://wordpress.org "WordPress")
<a name='安装依赖运行'></a>
  
## 安装依赖运行
根据实际情况调整配置设置选项  
如不需要调用`WordPress`请搜索`WordPress`并注释所有有关代码  
您可以设置`Crontab`以实现自动更新的功能
```bash
$ pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
$ python app.py
```

<a name='设置说明'></a>
  
## 设置说明
1.  apiUrl  
    该参数为[DXY-COVID-19-Crawler](https://github.com/BlankerL/DXY-COVID-19-Crawler "DXY-COVID-19-Crawler")
    提供的API。您也可以自行搭建。
2.  csvURL  
    由于API关闭了返回时间序列的功能，故需要下载数据仓库中提交的CSV文件以解析时间序列。若Github访问速度过慢，可以考虑使用提供的两个反向代理的地址。
3.  renderDir  
    最新数据的输出目录。该目录可以是相对路径、绝对路径  
4.  webRootDir
    输出路径对于网站根目录的相对路径。请保证该目录在网页环境下可读。
5.  xmlRPC
    WordPress的xmlrpc网址
6.  wpUser
    WordPress用户名
7.  wpPassword
    WordPress密码

<a name='开发说明'></a>
  
## 开发说明

<a name='app.py函数说明'></a>  
  
### app.py 函数说明  
  
<a name='基础函数'></a>  
  
#### 基础函数
  
<a name='需要手动调用的函数'></a>  
  
##### 需要手动调用的函数
  
<a name='getHistory'></a>  
  
###### getHistory()
功能：拉取并更新DXYOverall.txt  
⚠️ 此函数为`createDatazone()`的基础，请在主程序部分调用至少一次
使用了md5判断文件是否发生变化
  
<a name='fetchOverallData'></a>  
  
###### fetchOverallData()
功能：拉取最新OverallData  
⚠️ 此函数为`createIncreaseCharts()`及`createGeneralCharts()`基础，请在主程序部分调用至少一次  
⚠️ 请勿短时间内多次调用，否则API会返回503，请使用`time.sleep(2)`等函数
Update: 由于API经常崩，增加了异常处理。再无法拉取数据时从数据仓库中拉取。
  
<a name='fetchAreaData'></a>  
  
###### fetchAreaData()
功能：拉取最新省级信息  
⚠️ 此函数为省级疫情地图、条形图基础，请在主程序部分调用至少一次  
⚠️ 请勿短时间内多次调用，否则API会返回503，请使用`time.sleep(2)`等函数
Update: 由于API经常崩，增加了异常处理。再无法拉取数据时从数据仓库中拉取。
  
<a name='无需手动调用的函数'></a>  
  
##### 无需手动调用的函数
  
<a name='getFileMD5'></a>  
  
###### getFileMD5(filename)
功能：获取文件的md5值以便比较文件是否发生变化  
例如 `getFileMD5("README.md")`
  
<a name='timeStamp'></a>  

##### timeStamp(timeNum)
功能：将api中返回的ms单位Unix时间戳转为用户友好的格式  
例如`timeStamp(1593687723723)`

<a name='getOverallData'></a>  

###### getOverallData(data_parameter, region='domestic')
region变量默认"domenstic"，查询国内数据/疫情概括可省略此参数
若第二个变量不为空/domestic/global/general，则返回null
调用方法：
例如`getOverallData("currentConfirmedCount", "global")`
返回：`4599539` 全球现存确诊人数
例如`getOverallData("currentConfirmedCount")`
返回：`509` 国内现存确诊人数

| 变量1 | 变量2 |注释 |
| :------------: | :------------: | :------------: |
| currentConfirmedCount  | domestic | 国内现存确诊人数 |
| currentConfirmedIncr | domestic | 国内现存确诊人数增量（较昨日） |
| confirmedCount | domestic | 国内确诊人数（累计） |
| confirmedIncr | domestic | 国内确诊人数增量（较昨日） |
| suspectedCount  | domestic | 国内疑似感染人数 |
| suspectedIncr  | domestic | 国内疑似感染人数增量 |
| curedCount | domestic | 国内治愈人数（累计） |
| curedIncr | domestic | 国内治愈人数（增量） |
| deadCount | domestic | 国内死亡人数（累计） |
| deadIncr | domestic | 国内死亡人数增量 |
| seriousCount | domestic | 国内现存重症病例人数 |
| seriousIncr | domestic | 国内重症病例人数增量 |
| | | |
| currentConfirmedCount  | global | 全球现存确诊人数 |
| confirmedCount | global | 全球确诊人数（累计） |
| curedCount | global | 全球治愈人数（累计） |
| deadCount  | global | 全球死亡人数（累计） |
| currentConfirmedIncr | global | 全球现存确诊人数增量（较昨日） |
| confirmedIncr  | global | 全球累计确诊人数增量（较昨日） |
| curedIncr | global | 全球治愈人数增量（较昨日） |
| deadIncr | global | 全球死亡人数增量（较昨日） |
| | | |
| generalRemark | general | 全国疫情信息概览 |
| remarkX | general | 注释内容 (X 为 1-5) |
| note1 | general | 病毒名称 |
| note2 | general | 传染源 |
| note3 | general | 传播途径 |
| updateTime | general | 数据最后变动时间 |

`updateTime` 返回值为Unix时间戳，单位ms
  
<a name='historyReader'></a>  
  
###### historyReader(parameter)
`parameter`取值列表`['updateTime', 'confirmedCount', 'curedCount', 'deadCount']`  
功能：读取数据仓库中拉取的CSV历史数据，拉取表头为`parameter`的列，并以`'confirmedCount', 'curedCount', 'deadCount'`为关键词去重  
~~注意，此方法会使数据不严谨~~  
Update: 已解决  
⚠️：依赖`getHistory()`所提供的文件，若不执行可能导致数据过旧，请确认在主程序中先执行过此函数  
返回值类型为列表  
例如：`historyReader('confirmedCount')`
  
<a name='getAreaData'></a>  
  
###### getAreaData(parameter='confirmedCount')
功能：返回一个包含所有省份数据（元组形式）的list，倒序排列  
`parameter`可选列表`['currentConfirmedCount', 'confirmedCount', 'curedCount', 'deadCount']`
注意
例如`getAreaData()`  
返回值`[('湖北', 68135), 省略,  ('西藏', 1)]`  
  
<a name='绘图函数'></a>  
  
#### 绘图函数

<a name='createGeneralCharts'></a>  
  
##### createGeneralCharts()
功能：绘制当前疫情数据总表，无需输入参数。  
⚠️：依赖`fetchOverallData()`所提供的数据，请确认在主程序中先执行过此函数  
类型`Bar`
输出文件为`"%s/bar_general.html" % renderDir`
  
<a name='createIncreaseCharts'></a>  
  
##### createIncreaseCharts()
功能：绘制昨日疫情变化，无需输入参数。  
⚠️：依赖`fetchOverallData()`所提供的数据，请确认在主程序中先执行过此函数  
类型`Bar`  
输出文件为`"%s/bar_increase.html" % renderDir`
  
<a name='createDatazone'></a>  
  
##### createDatazone()
功能：绘制国内疫情变化时间轴
类型`Line`平滑曲线  
⚠️：依赖`getHistory()`所提供的文件，若不执行可能导致数据过旧，请确认在主程序中先执行过此函数  
开启了x轴数据缩放（`datazoom_opts`）  
输出文件为`"%s/bar_datazoom.html" % renderDir`

<a name='createProvincialBar'></a>  

##### createProvincialBar()
功能：绘制国内省级现存确诊条形图
类型`Bar`  
仅输出有现存确诊病例的省份  
输出文件`"%s/bar_provincial.html" % renderDir`

<a name='createMap'></a>  

##### createMap()
功能：绘制国内疫情地图。包含累计确诊、治愈、死亡  
类型`Map`  
输出文件`%s/map.html" % renderDir`

<a name='功能函数'></a>  
  
#### 功能函数
暂无
  
<a name='参数说明'></a>  

### 参数说明

<a name='fileChange'></a>  

#### fileChange
一个判断是否有数据发生改变的参数，初始值`False`，一旦检测出任何源数据变化，则返回`True`，以便后续重新生成网页


<a name='historyUpdated'></a>  


#### historyUpdated
一个判断CSV文件是否发生变化的函数，用于沟通`getHistory()`和`fileChange`参数。  
初始值`False`，一旦确认CSV文件更新，则变成`True`，并将`fileChange`赋值`True`

<a name='参与贡献'></a>  

## 参与贡献

1.  Clone 本仓库
2.  切换至Develop分支
3.  提交代码至Develop分支
4.  新建 Pull Request
